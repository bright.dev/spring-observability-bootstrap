FROM bellsoft/liberica-openjdk-alpine:17
ARG JAR_FILE=build/libs/\*.jar
COPY ${JAR_FILE} app.jar
ADD https://github.com/open-telemetry/opentelemetry-java-instrumentation/releases/download/v1.26.0/opentelemetry-javaagent.jar /opt/opentelemetry-agent.jar
ENTRYPOINT ["java", "-javaagent:/opt/opentelemetry-agent.jar", "-Dio.opentelemetry.javaagent.slf4j.simpleLogger.defaultLogLevel=ERROR","-jar","/app.jar"]
