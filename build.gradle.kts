import org.springframework.boot.gradle.plugin.SpringBootPlugin

allprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.jetbrains.kotlin.plugin.jpa")
    apply(plugin = "org.jetbrains.kotlin.plugin.spring")

    repositories { mavenCentral() }

    dependencies {
        implementation(platform(SpringBootPlugin.BOM_COORDINATES))
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }
}

plugins {
    id("org.springframework.boot") version "3.2.1"
    kotlin("jvm")
    kotlin("plugin.jpa")
    kotlin("plugin.spring")
}

group = "dev.bright"
version = "0.0.1-SNAPSHOT"

dependencies {
    implementation(project(":database"))

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.postgresql:postgresql")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.getByName<Jar>("jar") {
    enabled = false
}
