package dev.bright.spring.observability.bootstrap.appointment

import dev.bright.spring.observability.bootstrap.database.testfixtures.PostgresTestContainer
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.nulls.shouldNotBeNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.time.OffsetDateTime

@ActiveProfiles("test", "database")
@SpringBootTest(classes = [AppointmentTestConfiguration::class])
@PostgresTestContainer
internal class AppointmentServiceTest {

    @Autowired
    private lateinit var appointmentService: AppointmentService

    @Test
    fun createAppointmentTest() {
        val appointment = appointmentService.createAppointment(createAppointmentRequest())
        appointment.id.shouldNotBeNull()
    }

    @Test
    fun getAppointmentsTest() {
        appointmentService.createAppointment(createAppointmentRequest())

        val appointments = appointmentService.getAppointments()
        appointments.shouldNotBeEmpty()
    }

    @Test
    fun getAppointmentTest() {
        val appointment = appointmentService.createAppointment(createAppointmentRequest())

        val appointmentFromDb = appointmentService.getAppointment(appointment.id)
        appointmentFromDb.shouldNotBeNull()
    }

    private fun createAppointmentRequest() = CreateAppointmentRequest(
        start = OffsetDateTime.now(),
        end = OffsetDateTime.now().plusHours(1),
        description = "Test Appointment",
    )
}