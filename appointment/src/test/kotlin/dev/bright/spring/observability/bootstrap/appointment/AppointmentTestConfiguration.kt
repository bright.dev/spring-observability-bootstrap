package dev.bright.spring.observability.bootstrap.appointment

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.TestConfiguration

@TestConfiguration
@SpringBootApplication(scanBasePackages = ["dev.bright.spring.observability.bootstrap.appointment"])
internal class AppointmentTestConfiguration