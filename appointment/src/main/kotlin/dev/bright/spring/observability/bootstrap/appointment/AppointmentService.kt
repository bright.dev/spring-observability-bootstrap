package dev.bright.spring.observability.bootstrap.appointment

import org.springframework.stereotype.Service
import java.time.OffsetDateTime
import java.util.*

@Service
internal class AppointmentService(
    private val appointmentJpaRepository: AppointmentJpaRepository,
) {
    fun createAppointment(request: CreateAppointmentRequest): Appointment {
        val appointment = Appointment(
            startTime = request.start,
            endTime = request.end,
            description = request.description,
        )
        return appointmentJpaRepository.save(appointment)
    }

    fun getAppointments(): List<Appointment> {
        return appointmentJpaRepository.findAll()
    }

    fun getAppointment(id: UUID): Appointment {
        return appointmentJpaRepository.findById(id).orElseThrow()
    }
}

data class CreateAppointmentRequest(
    val start: OffsetDateTime,
    val end: OffsetDateTime,
    val description: String,
)