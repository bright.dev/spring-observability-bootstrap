package dev.bright.spring.observability.bootstrap.appointment

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/appointments")
internal class AppointmentController(
    private val appointmentService: AppointmentService,
) {
    @GetMapping
    fun getAppointments(): List<Appointment> {
        return appointmentService.getAppointments()
    }

    @GetMapping("/{id}")
    fun getAppointments(id: UUID): Appointment {
        return appointmentService.getAppointment(id)
    }
}