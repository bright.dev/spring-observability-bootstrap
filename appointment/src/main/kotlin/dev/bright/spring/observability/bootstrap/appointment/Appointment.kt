package dev.bright.spring.observability.bootstrap.appointment

import jakarta.persistence.Entity
import jakarta.persistence.Id
import java.time.OffsetDateTime
import java.util.*

@Entity
class Appointment(
    val startTime: OffsetDateTime,
    val endTime: OffsetDateTime,
    val description: String,
) {
    @Id
    val id: UUID = UUID.randomUUID()
}
