package dev.bright.spring.observability.bootstrap.appointment

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

internal interface AppointmentJpaRepository : JpaRepository<Appointment, UUID>