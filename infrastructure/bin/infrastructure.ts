#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import {InfrastructureStack} from '../lib/infrastructure-stack';
import {projectEnvSpecificName} from "../lib/env-utils";
import {NetworkStack} from "../lib/network-stack";
import {DatabaseStack} from "../lib/database-stack";

async function main() {
    const app = new cdk.App();
    const network = new NetworkStack(app, projectEnvSpecificName("network"));
    const database = new DatabaseStack(app, projectEnvSpecificName("database"), network.vpc)
    new InfrastructureStack(app, projectEnvSpecificName("app-service"), {
        dbInstance: database.databaseInstance,
        vpc: network.vpc
    })
}

main().catch(er => {
    console.log(er)
    process.exit(1)
})