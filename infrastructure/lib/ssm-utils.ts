import {Construct} from "constructs";
import * as ssm from "aws-cdk-lib/aws-ssm";
import {StringParameter} from "aws-cdk-lib/aws-ssm";
import {IStringParameter} from "aws-cdk-lib/aws-ssm/lib/parameter";
import {deployEnv} from "./env-utils";


export function getSecureSsmParameter(scope: Construct, fullParameterPath: string, version = 1): IStringParameter {
    ensureValidParameterPath(fullParameterPath)
    return StringParameter.fromSecureStringParameterAttributes(scope, fullParameterPath, {
        parameterName: fullParameterPath,
        version
    });
}

export function getSecureSsmParameterForEnv(scope: Construct, parameterPath: string, version = 1): IStringParameter {
    ensureValidParameterPath(parameterPath)
    return StringParameter.fromSecureStringParameterAttributes(scope, `/${deployEnv()}${parameterPath}`, {
        parameterName: `/${deployEnv()}${parameterPath}`,
        version
    });
}

export function getSsmParameterForEnv(scope: Construct, parameterPath: string): string {
    ensureValidParameterPath(parameterPath)
    return ssm.StringParameter.valueForStringParameter(scope, `/${deployEnv()}${parameterPath}`)
}

function ensureValidParameterPath(parameterPath: string) {
    if (!parameterPath.startsWith("/")) {
        throw new Error(`Parameter path must start with / got: ${parameterPath}`)
    }
}