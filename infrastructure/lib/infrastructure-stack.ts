import {Duration, Stack} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {
    AwsLogDriver,
    Cluster,
    ContainerDefinitionOptions,
    ContainerImage,
    FargateService,
    FargateTaskDefinition,
    LogDrivers,
    Secret,
    TaskDefinition
} from "aws-cdk-lib/aws-ecs";
import {LogGroup} from "aws-cdk-lib/aws-logs";
import {IVpc, Port} from "aws-cdk-lib/aws-ec2";
import * as ecr from "aws-cdk-lib/aws-ecr";
import {deployEnv, projectEnvSpecificName} from "./env-utils";
import {DatabaseInstance} from "aws-cdk-lib/aws-rds";
import {DatabaseStack, removalPolicyAppropriateForEnv} from "./database-stack";
import {
    ApplicationLoadBalancer,
    ApplicationProtocol,
    IApplicationLoadBalancerTarget
} from "aws-cdk-lib/aws-elasticloadbalancingv2";
import {getSecureSsmParameterForEnv, getSsmParameterForEnv} from "./ssm-utils";


interface AppStackProps {
    vpc: IVpc,
    dbInstance: DatabaseInstance
}

export class InfrastructureStack extends Stack {

    private readonly TAG_COMMIT: string = process.env.TAG_COMMIT || 'latest'
    private readonly ECR_REPOSITORY_NAME: string = "spring-observability-bootstrap-app"
    AWS_REGION: string = process.env.AWS_REGION || 'eu-central-1'
    private readonly containerPort = 8080;

    constructor(scope: Construct, id: string, props: AppStackProps) {
        super(scope, id);
        const fargateService = this.fargateService(props.vpc, props.dbInstance);
        this.loadBalancer(props.vpc, fargateService);
    }

    private fargateService(vpc: IVpc, dbInstance: DatabaseInstance): FargateService {
        const cluster = new Cluster(this, projectEnvSpecificName('Cluster'), {
            vpc: vpc,
        });
        const taskDefinition = this.taskDefinition(dbInstance);

        const fargateService = new FargateService(this, projectEnvSpecificName('fargate-service'), {
            cluster: cluster,
            taskDefinition: taskDefinition,
            desiredCount: 2,
            healthCheckGracePeriod: Duration.seconds(220)

        });
        const scalableTaskCount = fargateService.autoScaleTaskCount({
            minCapacity: 2,
            maxCapacity: 4
        });

        scalableTaskCount.scaleOnCpuUtilization(projectEnvSpecificName("service-auto-scaling"), {
            targetUtilizationPercent: 50,
            scaleInCooldown: Duration.seconds(150),
            scaleOutCooldown: Duration.seconds(150),
        })
        fargateService.connections.allowTo(dbInstance.connections, Port.tcp(DatabaseStack.databasePort), "Postgres db connection")
        return fargateService;
    }

    private taskDefinition(dbInstance: DatabaseInstance): TaskDefinition {
        const taskDefinition = new FargateTaskDefinition(this, projectEnvSpecificName('task-definition'), {
            cpu: 512,
            memoryLimitMiB: 1024,
        });

        taskDefinition.addContainer(projectEnvSpecificName("task-definition-container"), this.bounitesAppContainerDefinitionOptions(dbInstance))
        taskDefinition.addContainer(projectEnvSpecificName("otel-collector"), this.otelCollectorContainerDefinitionOptions())
        return taskDefinition;
    }

    private bounitesAppContainerDefinitionOptions(dbInstance: DatabaseInstance) {
        return {
            containerName: projectEnvSpecificName("ecs-container"),
            image: ContainerImage.fromEcrRepository(ecrRepositoryForService(this, this.ECR_REPOSITORY_NAME), this.TAG_COMMIT),
            portMappings: [{
                containerPort: this.containerPort
            }],
            logging: new AwsLogDriver({
                logGroup: new LogGroup(this, projectEnvSpecificName("log-group"), {
                    logGroupName: projectEnvSpecificName("log-group"),
                    removalPolicy: removalPolicyAppropriateForEnv()
                }),
                streamPrefix: projectEnvSpecificName("-"),
            }),
            environment: {
                DB_POSTGRES_HOST: dbInstance.dbInstanceEndpointAddress,
                DB_POSTGRES_PORT: dbInstance.dbInstanceEndpointPort,
                DB_POSTGRES_NAME: DatabaseStack.databaseName,
                OTEL_TRACES_SAMPLER: "always_on",
                OTEL_PROPAGATORS: "tracecontext,baggage",
                OTEL_METRICS_EXPORTER: "otlp",
                OTEL_SERVICE_NAME: projectEnvSpecificName(),
            },
            secrets: {
                DB_POSTGRES_PASSWORD: Secret.fromSecretsManager(dbInstance.secret!, "password"),
                DB_POSTGRES_USERNAME: Secret.fromSecretsManager(dbInstance.secret!, "username"),
            },
        };
    }

    private otelCollectorContainerDefinitionOptions(): ContainerDefinitionOptions {
        const otelCollectorLogGroup = new LogGroup(
            this, projectEnvSpecificName("otel-log-group"),
            {
                logGroupName: projectEnvSpecificName("otel-log-group"),
                removalPolicy: removalPolicyAppropriateForEnv()
            }
        );

        return {
            image: ContainerImage.fromEcrRepository(ecrRepositoryForService(this, "otel-collector")),
            memoryReservationMiB: 512,
            logging: LogDrivers.awsLogs({
                streamPrefix: projectEnvSpecificName('otel-collector'),
                logGroup: otelCollectorLogGroup,
            }),
            environment: {
                REGION: this.AWS_REGION,
                DEPLOY_ENV: deployEnv(),
                TEMPO_ENDPOINT: getSsmParameterForEnv(this, "/backend/TEMPO_ENDPOINT"),
                PROM_ENDPOINT: getSsmParameterForEnv(this, "/backend/PROM_ENDPOINT"),
            }, secrets: {
                TEMPO_USERNAME: Secret.fromSsmParameter(getSecureSsmParameterForEnv(this, "/backend/TEMPO_USERNAME")),
                TEMPO_PASSWORD: Secret.fromSsmParameter(getSecureSsmParameterForEnv(this, "/backend/TEMPO_PASSWORD")),
                PROM_USERNAME: Secret.fromSsmParameter(getSecureSsmParameterForEnv(this, "/backend/PROM_USERNAME")),
                PROM_PASSWORD: Secret.fromSsmParameter(getSecureSsmParameterForEnv(this, "/backend/PROM_PASSWORD")),
            }
        };
    }


    private loadBalancer(vpc: IVpc, targetService: IApplicationLoadBalancerTarget) {
        const loadBalancer = new ApplicationLoadBalancer(this, projectEnvSpecificName("load-balancer"), {
            vpc: vpc,
            internetFacing: true
        });

        const httpListener = loadBalancer.addListener("http", {
            open: true,
            protocol: ApplicationProtocol.HTTP
        })

        const apiHttpsTargetGroup = httpListener.addTargets('api', {
            port: this.containerPort,
            deregistrationDelay: Duration.seconds(10),
            targets: [targetService],
            healthCheck: {
                healthyThresholdCount: 2,
                unhealthyThresholdCount: 2,
                path: "/actuator/health",
                healthyHttpCodes: "200",
            }
        });
    }
}

export function ecrRepositoryForService(scope: Construct, serviceName: string) {
    return ecr.Repository.fromRepositoryName(scope, `${serviceName} repository`, serviceName)
}
