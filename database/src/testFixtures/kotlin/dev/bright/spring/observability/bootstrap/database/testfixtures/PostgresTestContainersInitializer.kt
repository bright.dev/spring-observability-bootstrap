package dev.bright.spring.observability.bootstrap.database.testfixtures

import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.ContextConfiguration
import org.testcontainers.containers.PostgreSQLContainer

@ContextConfiguration(initializers = [PostgresTestContainersInitializer::class])
annotation class PostgresTestContainer

class PostgresTestContainersInitializer :
    ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        val postgresSqlContainer = PostgreSQLContainer<Nothing>("postgres:15.4")

        postgresSqlContainer.start()

        // should shut down container on context close
        applicationContext.beanFactory.registerSingleton("postgresSqlContainer", postgresSqlContainer)

        TestPropertyValues.of(
            mapOf(
                "spring.datasource.url" to postgresSqlContainer.jdbcUrl,
                "spring.datasource.username" to postgresSqlContainer.username,
                "spring.datasource.password" to postgresSqlContainer.password,
            )
        ).applyTo(applicationContext)
    }
}
