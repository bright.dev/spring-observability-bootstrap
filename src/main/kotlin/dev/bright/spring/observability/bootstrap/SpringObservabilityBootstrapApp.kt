package dev.bright.spring.observability.bootstrap

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpringObservabilityBootstrapApp {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(SpringObservabilityBootstrapApp::class.java, *args)
        }
    }
}