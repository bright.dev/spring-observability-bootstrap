package dev.bright.spring.observability.bootstrap.greetings

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class GreetingController {

    @GetMapping("/greetings")
    fun greetings(): Greetings {
        return Greetings("Hello my friend")
    }

    data class Greetings(val message: String)
}